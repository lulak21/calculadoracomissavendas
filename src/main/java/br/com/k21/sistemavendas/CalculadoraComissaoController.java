package br.com.k21.sistemavendas;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculadoraComissaoController {

    @RequestMapping("/calculo-comissao")
    public double greeting(@RequestParam(value="valorVenda", defaultValue="0") double valorVenda) {
        return CalculadoraComissao.calcular(valorVenda);
    }
}
