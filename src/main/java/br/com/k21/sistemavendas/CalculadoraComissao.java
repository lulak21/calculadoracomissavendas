package br.com.k21.sistemavendas;

public class CalculadoraComissao {

	private static final double COMISSAO_FAIXA1 = 0.05;
	private static final double COMISSAO_FAIXA2 = 0.06;
	private static final int VALOR_LIMITE_SUPERIOR_FAIXA1 = 10000;

	public static double calcular(double venda) {

		if (venda <= VALOR_LIMITE_SUPERIOR_FAIXA1) {
			return truncar(venda * COMISSAO_FAIXA1);
		}

		return truncar(venda * COMISSAO_FAIXA2);
	}

	private static double truncar(double valor) {
		return Math.floor(valor * 100) / 100;
	}
}
