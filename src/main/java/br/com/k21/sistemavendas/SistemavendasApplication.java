package br.com.k21.sistemavendas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemavendasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemavendasApplication.class, args);
	}
}
