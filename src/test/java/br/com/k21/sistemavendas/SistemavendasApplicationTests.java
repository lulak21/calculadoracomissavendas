package br.com.k21.sistemavendas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SistemavendasApplicationTests {

	@Test
	public void teste_calcular_comissao_com_venda_5k_retorna_250() {
		double venda = 5000;
		double comissao = 250;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}	

	//TODO Arrumar com BigDecimal a implementacao
	public void teste_calcular_comissao_com_venda_10001_retorna_600_ponto_06() {
		double venda = 10001;
		double comissao = 600.06;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}
	
	@Test
	public void teste_venda_10k() {
		double venda = 10000;
		double comissao = 500;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}
	
	@Test
	public void teste_venda_15k() {
		double venda = 15000;
		double comissao = 900;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}
	
	@Test
	public void teste_venda_59k() {
		double venda = 55.59;
		double comissao = 2.77;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}
	
	@Test
	public void teste_venda_15k_ponto_59() {
		double venda = 15000.59;
		double comissao = 900.03;
		double resultado = CalculadoraComissao.calcular(venda);
		assertEquals(comissao,resultado, 0);
	}

}
