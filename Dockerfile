FROM openjdk:8-jdk-alpine

COPY target/sistemavendas-0.0.1.jar .

ENTRYPOINT ["java", "-jar", "./sistemavendas-0.0.1.jar"]